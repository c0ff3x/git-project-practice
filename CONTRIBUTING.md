# Contribute

If you want to contribute please make a merge request with your code with a short description about "what does" your code.

If you see any problem you can use Gitlab's [issue tracker](https://gitlab.com/c0ff3x/git-project-practice/-/issues).
