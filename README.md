# Hunter

Hunter is a Java file explorer. The project will be develop under CLI first and then a UI, with *Hunter* you can not only search for files you can also edit them, hide files inside another, protect them with password, duplicate them and more.

The project is under development if you want to contribute see Contributing file.

# License

This project uses GNU General Public License v3.0
